# OpenML dataset: abalone

https://www.openml.org/d/1557

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: [original](http://www.openml.org/d/183) - UCI    
**Please cite**:   

* Abstract: 

A 3-class version of abalone dataset.

* Sources:  

(a) Original owners of database: Marine Resources Division Marine Research Laboratories - Taroona Department of Primary Industry and Fisheries, Tasmania GPO Box 619F, Hobart, Tasmania 7001, Australia (contact: Warwick Nash +61 02 277277, wnash@dpi.tas.gov.au)

(b) Donor of database: Sam Waugh (Sam.Waugh@cs.utas.edu.au) Department of Computer Science, University of Tasmania GPO Box 252C, Hobart, Tasmania 7001, Australia

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1557) of an [OpenML dataset](https://www.openml.org/d/1557). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1557/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1557/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1557/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

